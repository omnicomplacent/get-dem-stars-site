class Score < Sequel::Model

  def validate
    super
    validates_presence :name
    validates_presence :value
  end

end