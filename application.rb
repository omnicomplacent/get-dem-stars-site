require 'roda'
require 'tilt'
require 'erubis'
require 'tilt/erubis'
require 'yaml'
require 'sequel'

DB_settings = YAML.load_file('database.yml')
database = DB_settings["default"]["database"]
user     = DB_settings["default"]["user"]
password = DB_settings["default"]["password"]
DB = Sequel.connect(adapter: "postgres", password: password, database: database, user: user)

require './models/score.rb'

class Application < Roda

  Sequel::Model.plugin :validation_helpers

  use Rack::Session::Cookie, :secret => '1dfsa3fs3fv23fwg343vb343erg43vw', key: '_super_awesome_session'

  plugin :static, ["/images", "/css", "/js"]
  plugin :render
  plugin :head
  plugin :flash

  #TODO add pagination to created questions (20 per page? less?)
  route do |r|

    # GET /
    r.root do
	  @scores = Score.all
      view "index"
    end

    r.is do
      r.get do
        @scores = Score.all
        view "index"
      end

	  r.post do
	    @score = Score.new(r["score"])
	  end
	end
  end
end
