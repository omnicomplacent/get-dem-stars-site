namespace :db do
  namespace :migrate do
    desc "Migrate Up"
    task :up => :app do
      Sequel.extension(:migration)
      Sequel::Migrator.run(Sequel::Model.db, "migrate")
      puts "<= Migration Up Completed"
    end
    desc "Migrate Down"
    task :down => :app do
      Sequel.extension(:migration)
      Sequel::Migrator.run(Sequel::Model.db, "migrate", target: 0)
      puts "<= Migration Down Completed"
    end
  end

  desc "Migrate Up"
  task :migrate => "db:migrate:up"

  #TODO creation doesn't create DB
  desc "Create DB"
  task :create => :app do
    config = Sequel::Model.db.opts
    config[:charset] = "utf8" unless config[:charset]
    puts "=> Creating database '#{config[:database]}'"
    create_db(config)
    puts "<= Database '#{config[:database]}' is ready for data (nom)"
  end

  desc "Drop DB"
  task :drop => :app do
    Sequel::Model.db.disconnect
    config = Sequel::Model.db.opts
    puts "=> Dropping database '#{config[:database]}'"
    drop_db(config)
    puts "<= Database '#{config[:database]}' Dropped (aww)"
  end
end

def self.create_db(config)
  environment = {}
  environment["PGUSER"]     = config[:user]
  environment["PGPASSWORD"] = config[:password]
  arguments = []
  arguments << "--encoding=#{config[:charset]}" if config[:charset]
  arguments << "--host=#{config[:host]}"        if config[:host]
  arguments << "--username=#{config[:user]}"    if config[:user]
  arguments << config[:database]
  Process.wait Process.spawn(environment, "createdb", *arguments)
end

def self.drop_db(config)
  environment = {}
  environment["PGUSER"]     = config[:user]
  environment["PGPASSWORD"] = config[:password]
  arguments = []
  arguments << "--host=#{config[:host]}" if config[:host]
  arguments << "--username=#{config[:user]}" if config[:user]
  arguments << config[:database]
  Process.wait Process.spawn(environment, "dropdb", *arguments)
end
