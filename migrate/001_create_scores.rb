Sequel.migration do
  change do
    create_table :scores do
      primary_key :id, unique: true
      String :name, null: false
      Integer :value, null: false
    end
  end
end